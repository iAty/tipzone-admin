import { Component, Input } from '@angular/core'

@Component({
    selector: 'tipzone-mfa',
    templateUrl: './mfa.html'
})
export class MFAComponent {
    @Input() destination: string
    @Input() whenSubmit: (code: string) => void

    constructor() {
        console.log('MFAComponent constructor')
    }

    onSubmit(code?: any): void {
      console.warn('MFA code', code)
    }
}
