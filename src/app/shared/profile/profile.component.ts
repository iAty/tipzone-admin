import { Component, Output } from '@angular/core'
import { CognitoUtil } from '../../services/user/cognito.service'
import { UserLoginService } from '../../services/user/user-login.service'

@Component({
    // moduleId: module.id,
    selector: 'tipzone-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent {
    parameters: any

    currentDate: Date = new Date()

    constructor(
      public userService: UserLoginService,
      public cognitoUtil: CognitoUtil) {
    }



}
