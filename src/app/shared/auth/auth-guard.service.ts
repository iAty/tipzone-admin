import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router'
import { Injectable } from '@angular/core'
import { AuthService } from './auth.service'
import { UserLoginService } from '../../services/user/user-login.service'
import { CognitoUtil, LoggedInCallback } from '../../services/user/cognito.service'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'

@Injectable()
export class AuthGuard implements CanActivate {
  isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

  constructor(private authService: UserLoginService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

  }


}
