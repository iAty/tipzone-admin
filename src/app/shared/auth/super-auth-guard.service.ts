import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router'
import { Injectable } from '@angular/core'
import { AuthService } from './auth.service'
import { UserLoginService } from '../../services/user/user-login.service'

@Injectable()
export class SuperAuthGuard implements CanActivate {

  constructor(private authService: UserLoginService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.isAuthenticated()) {
      return true
    }
    this.router.navigate(['/login'])
    return false
    // return true;
  }

}
