import { Component, OnInit } from '@angular/core'
import { UserLoginService } from '../../services/user/user-login.service'

@Component({
    selector: 'tipzone-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {


    constructor(private user: UserLoginService) {}

    ngOnInit() {

    }

    logout(e: Event): void {
      e.preventDefault()
      this.user.logout()
    }


}
