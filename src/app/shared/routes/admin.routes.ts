
import { Routes, RouterModule } from '@angular/router'

export const EVENTS_ROUTES: Routes = [
    {
        path: 'events',
        loadChildren: './pages/events/events.module#EventsPageModule'
      },
      {
        path: 'newsletter',
        loadChildren: './pages/newsletter/newsletter.module#NewsletterPageModule'
      },
]
