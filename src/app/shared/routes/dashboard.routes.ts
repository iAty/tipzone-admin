import { Routes, RouterModule } from '@angular/router'

export const DASHBOARD_ROUTES: Routes = [
    {
        path: 'dashboard',
        loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule'
      },
      {
        path: 'news',
        loadChildren: './pages/news/news.module#NewsPageModule'
      },
      {
        path: 'changelog',
        loadChildren: './changelog/changelog.module#ChangeLogModule'
      },
]
