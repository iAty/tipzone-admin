import { Routes, RouterModule } from '@angular/router'

export const GROUPS_ROUTES: Routes = [
    {
        path: 'groups',
        loadChildren: './pages/groups/groups.module#GroupsPageModule'
      },
]
