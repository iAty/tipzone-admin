import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { FooterComponent } from './footer/footer.component'
import { NavbarComponent } from './navbar/navbar.component'
import { SidebarComponent } from './sidebar/sidebar.component'
import { ToggleFullscreenDirective } from './directives/toggle-fullscreen.directive'
import { MFAComponent } from './mfa/mfa.component'
import { NgxDatatableModule } from '@swimlane/ngx-datatable'
import { ProfileComponent } from './profile/profile.component'
import { UserParametersService } from '../services/user/user-parameters.service'
import { HttpModule } from '@angular/http'
import { MyProfileComponent } from '../secure/profile/myprofile.component'
import { NgPipesModule } from 'ngx-pipes'

@NgModule({
  exports: [
    CommonModule,
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    MFAComponent,
    ToggleFullscreenDirective,
    NgxDatatableModule,
    NgbModule,
    ProfileComponent,
    MyProfileComponent,
    NgPipesModule
  ],
  imports: [
    RouterModule,
    CommonModule,
    NgxDatatableModule,
    NgbModule,
    HttpModule,
    NgPipesModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    MFAComponent,
    ToggleFullscreenDirective,
    ProfileComponent,
    MyProfileComponent
  ],
  providers: [UserParametersService]
})
export class SharedModule {}
