import { RouteInfo } from './sidebar.metadata'

export const ROUTES: RouteInfo[] = [
  {
    path: '/', title: 'Dashboard', icon: 'ft-home', class: 'nav-item',
    badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: []
  },
  {
    path: '/groups', title: 'Groups', icon: 'ft-users', class: 'nav-item',
    badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: []
  },
  {
    path: '/news', title: 'News', icon: 'ft-activity', class: 'nav-item',
    badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: []
  },
  {
    path: '/changelog', title: 'ChangeLog', icon: 'ft-file', class: 'nav-item',
    badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: []
  },
  {
    path: '/events', title: 'Events', icon: 'ft-settings', class: 'nav-item', hasSuper: true,
    badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: []
  },
  {
    path: '/newsletter', title: 'Newsletter', icon: 'ft-settings', class: 'nav-item', hasSuper: true,
    badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: []
  },
]

