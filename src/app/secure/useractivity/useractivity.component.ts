
import {Component} from '@angular/core'

import {Router} from '@angular/router'
import { DynamoDBService } from '../../services/user/ddb.service'
import { UserLoginService } from '../../services/user/user-login.service'
import { LoggedInCallback } from '../../services/user/cognito.service'


export class Stuff {
    public type: string
    public date: string
}

@Component({
    selector: 'tipzone-awscognito-useractivity',
    templateUrl: './useractivity.html'
})
export class UseractivityComponent implements LoggedInCallback {

    public logdata: Array<Stuff> = []

    constructor(public router: Router, public ddb: DynamoDBService, public userService: UserLoginService) {
        this.userService.isAuthenticated(this)
        console.log('in UseractivityComponent')
    }

    isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
        if (!isLoggedIn) {
            this.router.navigate(['/login'])
        } else {
            console.log('scanning DDB')
            this.ddb.getLogEntries(this.logdata)
        }
    }

}
