import { Component, ViewEncapsulation, Output } from '@angular/core'
import { Router } from '@angular/router'
import { UserLoginService } from '../../services/user/user-login.service'
import { CognitoUtil, Callback, LoggedInCallback } from '../../services/user/cognito.service'
import { UserParametersService } from '../../services/user/user-parameters.service'
import { environment } from '../../../environments/environment'
import * as AWS from 'aws-sdk'


@Component({
  selector: 'tipzone-awscognito-myprofile',
  templateUrl: './myprofile.html',
  encapsulation: ViewEncapsulation.None
})
export class MyProfileComponent implements LoggedInCallback {

  public parameters: any = {
    roles: []
  }

  public cognitoId: String

  constructor(
    public router: Router,
    public userService: UserLoginService,
    public userParams: UserParametersService,
    public cognitoUtil: CognitoUtil) {
    this.userService.isAuthenticated(this)
    // // console.log('In MyProfileComponent');
  }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (!isLoggedIn) {
      this.router.navigate(['/login'])
    } else {
      this.userParams.getParameters(new GetParametersCallback(this, this.cognitoUtil))

    }
  }
}

export class Parameters {
  name: string
  value: string
}

export class GetParametersCallback implements Callback {

  constructor(public me: MyProfileComponent, public cognitoUtil: CognitoUtil) {

  }

  callback() {

  }

  callbackWithParam(result: any) {
    // console.log(result);
    if (!result) { return }
    for (let i = 0; i < result.length; i++) {

      const parameter = {}
      parameter[result[i].getName()] = result[i].getValue()
      this.me.parameters = Object.assign({}, this.me.parameters, parameter)
    }
    const param = {}
    param['cognito ID'] = this.cognitoUtil.getCognitoIdentity()
    this.me.parameters = Object.assign({}, this.me.parameters, param)

    this.refreshAvatar(this.cognitoUtil.getCognitoIdentity())

    this.getGroups(this.cognitoUtil)

  }

  refreshAvatar(useUUID: string): void {
    // // console.log('get id', useUUID);
    const s3 = new AWS.S3({
      'params': {
        'Bucket': environment.userFilesBucket
      },
      'region': environment.bucketRegion
    })
    // // console.log('get avatar', s3);
      s3.getSignedUrl('getObject', { 'Key': 'protected/user/' + useUUID + '/avatar.jpg' }, (err, url) => {
        // // console.log('getSignedUrl avatar');
        if (err) { console.warn('Avatar error:', err); return }
        this.perloadImage(url).then( imageLoaded => {
          if (imageLoaded) {
            this.me.parameters = Object.assign({}, this.me.parameters, {avatar: url})
          }
        })

      })
  }

  perloadImage(imageSrc: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const img = new Image()
      img.onload = function() {
        resolve(img)
      }
      img.src = imageSrc
    })
  }

  getGroups(cognitoUtil: any): void {
    // console.log('cognitoUser', cognitoUtil);

    cognitoUtil.getCurrentUser().getSession( (err, session) => {
        // if (err) { console.warn(err); return; }
        this.me.parameters.roles = session.getIdToken().payload['cognito:groups']
    })
  }
}
