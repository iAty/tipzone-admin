import { NgModule } from '@angular/core'
import { RouterModule, Routes, PreloadAllModules } from '@angular/router'

import { AuthGuard } from './shared/auth/auth-guard.service'
import { LoginComponent } from './pages/login/login.component'
import { DashboardComponent } from './pages/dashboard/dashboard.component'
import { DASHBOARD_ROUTES } from './shared/routes/dashboard.routes'
import { GROUPS_ROUTES } from './shared/routes/groups.routes'
import { EVENTS_ROUTES } from './shared/routes/admin.routes'

import { FixedNavbarFooterLayoutComponent } from './layouts/fixed-navbar-footer-layout/fixed-navbar-footer-layout.component'


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FixedNavbarFooterLayoutComponent,
    data: { title: '' }, children: DASHBOARD_ROUTES
   },
   {
    path: '',
    component: FixedNavbarFooterLayoutComponent,
    data: { title: '' }, children: GROUPS_ROUTES
   },
   {
    path: '',
    component: FixedNavbarFooterLayoutComponent,
    data: { title: '' }, children: EVENTS_ROUTES
   },
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
