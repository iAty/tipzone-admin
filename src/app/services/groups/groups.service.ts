import {Injectable} from '@angular/core'
import {CognitoUtil} from '../user/cognito.service'
import {environment} from '../../../environments/environment'

import * as AWS from 'aws-sdk/global'
import * as DynamoDB from 'aws-sdk/clients/dynamodb'
import { Stuff } from '../../secure/useractivity/useractivity.component'

/**
 * Created by Vladimir Budilov
 */

@Injectable()
export class GroupsService {

    constructor(public cognitoUtil: CognitoUtil) {
        // console.log('GroupsService: constructor');
    }

    getAWS() {
        return AWS
    }

    getGroups(mapArray: Array<any>): Promise<any> {
        // console.log('GroupsService: reading from DDB with creds - ' + AWS.config.credentials);
        return new Promise( (resolve, reject) => {
          const params = {
              TableName: environment.GROUPS_TABLE_NAME,
              ScanFilter: {
                'userId': {
                  ComparisonOperator: 'EQ',
                  AttributeValueList: [this.cognitoUtil.getCognitoIdentity()]
                }
              }
          }

          const clientParams: any = {}
          if (environment.dynamodb_endpoint) {
              clientParams.endpoint = environment.dynamodb_endpoint
          }
          const docClient = new DynamoDB.DocumentClient(clientParams)
          const getQUery = docClient.scan(params).promise()
          getQUery.then( data => {
              if (data) {
                  // print all the movies
                  // console.log('GroupsService: Query succeeded.');
                  data.Items.forEach(function (logitem) {
                      mapArray.push(logitem)
                  })
                  resolve(mapArray)
              }
          }).catch( hasError => {
            console.error('GroupsService: Unable to query the table. Error JSON:', JSON.stringify(hasError, null, 2))
            reject(hasError)
          })
      })
    }

    getGroupsById(groupId: string, updatedAt: number, callback: any) {
        // console.log('GroupsService: reading from DDB with creds - ' + AWS.config.credentials);
        const params = {
            TableName: environment.GROUPS_TABLE_NAME,
            Key: {
              groupId: groupId,
              updatedAt: updatedAt
            }
        }

        const clientParams: any = {}
        if (environment.dynamodb_endpoint) {
            clientParams.endpoint = environment.dynamodb_endpoint
        }
        const docClient = new DynamoDB.DocumentClient(clientParams)
        docClient.get(params, onQuery)

        function onQuery(err, data) {
            if (err) {
                console.error('GroupsService: Unable to query the table. Error JSON:', JSON.stringify(err, null, 2))
            } else {
                // print all the movies
                // console.log('GroupsService: Get succeeded.', data);
                    callback(data.Item)

            }
        }
    }





}

