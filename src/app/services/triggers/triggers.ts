import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/concatAll'
import 'rxjs/add/operator/share'
import { CognitoUtil } from '../user/cognito.service'
import { AmplifyService } from 'aws-amplify-angular'




const triggerstoreFactory = (
  cognitoUtil: CognitoUtil,
  amplify: AmplifyService
) => {
  return new TriggersStore( cognitoUtil, amplify)
}

const displayFormat = 'YYYY-MM-DD'
declare var AWS: any

@Injectable()
export class TriggersStore {
  private _news: BehaviorSubject<any> = new BehaviorSubject([])
  private endpoint: string

  constructor(
    private cognitoUtil: CognitoUtil,
    public amplify: AmplifyService
  ) {
    this.endpoint = 'https://kdqspa2a35.execute-api.eu-west-2.amazonaws.com/Development'
  }

  // get news () { return Observable.create( fn => this._news.subscribe(fn) ) }

  runResultsUpdater(): Promise<any> {
    const apiName = 'updateData'
    const path = '/results'
    const myInit = { // OPTIONAL
        headers: {}, // OPTIONAL
        response: true, // OPTIONAL (return entire response object instead of response.data)
        queryStringParameters: {} // OPTIONAL
    }
    return this.amplify.api().get(apiName, path, myInit)
    // return this.sigv4.get(this.endpoint, 'results', cognitoUser);
  }
}

export const TriggersStoreProvider = {
  provide: TriggersStore,
  useFactory: triggerstoreFactory,
  deps: [CognitoUtil, AmplifyService]
}

