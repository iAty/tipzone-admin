import {Injectable} from '@angular/core'
import {Callback, CognitoUtil} from './cognito.service'
// import { Sigv4Http } from '../sig4.service';

import { environment } from '../../../environments/environment'
import { Observable } from 'rxjs/Observable'

@Injectable()
export class UserParametersService {

    constructor(public cognitoUtil: CognitoUtil) {
    }

    getParameters(callback: Callback) {
        const cognitoUser = this.cognitoUtil.getCurrentUser()

        if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    // console.log('UserParametersService: Couldn\'t retrieve the user');
                } else {
                    cognitoUser.getUserAttributes(function (usererr, result) {
                        if (err) {
                            // console.log('UserParametersService: in getParameters: ' + usererr);
                        } else {
                            callback.callbackWithParam(result)
                        }
                    })
                }

            })
        } else {
            callback.callbackWithParam(null)
        }


    }

    getUser(callback: Callback) {
        const cognitoUser = this.cognitoUtil.getCurrentUser()

        if (cognitoUser != null) {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    // console.log('UserParametersService: Couldn\'t retrieve the user');
                } else {
                    cognitoUser.getUserAttributes(function (usererr, result) {
                        if (err) {
                            // console.log('UserParametersService: in getParameters: ' + usererr);
                        } else {
                            callback.callbackWithParam(result)
                        }
                    })
                }

            })
        } else {
            callback.callbackWithParam(null)
        }


    }
}
