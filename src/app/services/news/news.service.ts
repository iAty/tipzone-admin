import {Injectable} from '@angular/core'
import {CognitoUtil} from '../user/cognito.service'
import {environment} from '../../../environments/environment'

import * as AWS from 'aws-sdk/global'
import * as DynamoDB from 'aws-sdk/clients/dynamodb'
import { Stuff } from '../../secure/useractivity/useractivity.component'

/**
 * Created by Vladimir Budilov
 */

@Injectable()
export class NewsService {

    constructor(public cognitoUtil: CognitoUtil) {
        // console.log('GroupsService: constructor');
    }

    getAWS() {
        return AWS
    }

    getNewsByUser(): Promise<any> {
        // console.log('GroupsService: reading from DDB with creds - ' + AWS.config.credentials);
        return new Promise( (resolve, reject) => {


        const params = {
            TableName: environment.GROUP_NEWS_TABLE,
            ScanFilter: {
              'userId': {
                ComparisonOperator: 'EQ',
                AttributeValueList: [this.cognitoUtil.getCognitoIdentity()]
              }
            }
        }

        const clientParams: any = {}
        if (environment.dynamodb_endpoint) {
            clientParams.endpoint = environment.dynamodb_endpoint
        }
        const docClient = new DynamoDB.DocumentClient(clientParams)
        docClient.scan(params).promise().then( gotData => {
          resolve(gotData.Items)
        }).catch( hasError => reject(hasError))

        // function onQuery(err, data) {
        //     if (err) {
        //         console.error('getNewsByUser: Unable to query the table. Error JSON:', JSON.stringify(err, null, 2));
        //     } else {
        //         // print all the movies
        //         // console.log('GroupsService: Query succeeded.');
        //         // mapArray = [];
        //         data.Items.forEach(function (logitem) {
        //             mapArray.push(logitem);
        //         });
        //     }
        // }
      })
    }

    createNews(item: any, callback: any) {
        // console.log('GroupsService: reading from DDB with creds - ' + AWS.config.credentials);
        item.updateAt = +item.updateAt
        item.createdAt = +item.createdAt
        const params = {
            TableName: environment.GROUP_NEWS_TABLE,
            Item: item
        }

        const clientParams: any = {}
        if (environment.dynamodb_endpoint) {
            clientParams.endpoint = environment.dynamodb_endpoint
        }
        const docClient = new DynamoDB.DocumentClient(clientParams)
        docClient.put(params, onQuery)

        function onQuery(err, data) {
            if (err) {
                console.error('createNews: Unable to query the table. Error JSON:', JSON.stringify(err, null, 2))
                callback(true)
            } else {
                // print all the movies
                console.log('createNews: Query succeeded.')
                callback(false)
            }
        }
    }

    // getGroupsById(groupId: string, updatedAt: number, callback: any) {
    //     // console.log('GroupsService: reading from DDB with creds - ' + AWS.config.credentials);
    //     const params = {
    //         TableName: environment.GROUPS_TABLE_NAME,
    //         Key: {
    //           groupId: groupId,
    //           updatedAt: updatedAt
    //         }
    //     };

    //     const clientParams: any = {};
    //     if (environment.dynamodb_endpoint) {
    //         clientParams.endpoint = environment.dynamodb_endpoint;
    //     }
    //     const docClient = new DynamoDB.DocumentClient(clientParams);
    //     docClient.get(params, onQuery);

    //     function onQuery(err, data) {
    //         if (err) {
    //             console.error('GroupsService: Unable to query the table. Error JSON:', JSON.stringify(err, null, 2));
    //         } else {
    //             // print all the movies
    //             // console.log('GroupsService: Get succeeded.', data);
    //                 callback(data.Item);

    //         }
    //     }
    // }





}

