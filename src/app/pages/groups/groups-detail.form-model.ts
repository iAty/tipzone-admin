import {
  DynamicFormControlModel,
  DynamicCheckboxModel,
  DynamicInputModel,
  DynamicSelectModel,
  DynamicRadioGroupModel,
  DynamicFormGroupModel,
  DynamicTextAreaModel
} from '@ng-dynamic-forms/core'
import { of } from 'rxjs/observable/of'

export const MY_FORM_MODEL: DynamicFormControlModel[] = [
  new DynamicFormGroupModel({
    id: 'group',
    legend: 'Group',
    group: [
      new DynamicInputModel({
        id: 'createdAt',
        disabled: true,
        label: 'Created At',
        placeholder: 'date'
      }),
      new DynamicInputModel({
        id: 'updatedAt',
        disabled: true,
        label: 'Updated at',
        placeholder: 'date'
      }),
      new DynamicRadioGroupModel({
        id: 'featured',
        label: 'Featured',
        disabled: true,
        options: [
          {
              label: 'Yes',
              value: true,
          },
          {
              label: 'No',
              value: false
          }
      ],
    }),
      new DynamicInputModel({
        id: 'groupId',
        disabled: true,
        label: 'Group id',
        placeholder: 'UUID'
      }),
      new DynamicInputModel({
        id: 'groupName',
        label: 'Group name',
        placeholder: 'name',
        validators: {
          required: null
        },
        errorMessages: {
            required: '{{ label }} is required'
        }
      }),
      new DynamicInputModel({
        id: 'brandcolor',
        label: 'Brand color',
        placeholder: 'color code'
      }),
      new DynamicRadioGroupModel({
        id: 'hasCover',
        label: 'Has cover',
        disabled: true,
        options: [
          {
              label: 'Yes',
              value: true,
          },
          {
              label: 'No',
              value: false
          }
      ],
    }),
    new DynamicInputModel({
      id: 'cover_file',
      label: 'Cover file',
      placeholder: 'file',
      inputType: 'file'
    }),
      new DynamicInputModel({
        id: 'logo',
        label: 'Brand logo',
        placeholder: 'logo'
      }),
      new DynamicInputModel({
        id: 'logo_file',
        label: 'Logo file',
        placeholder: 'file',
        inputType: 'file'
      }),
      new DynamicSelectModel<string>({
        id: 'groupType',
        label: 'Type',
        options: of([
            {
                label: 'PUBLIC',
                value: 'PUBLIC',
            },
            {
                label: 'PRIVATE',
                value: 'PRIVATE'
            },
            {
                label: 'CLOSED',
                value: 'CLOSED'
            }
        ])
    }),
    new DynamicRadioGroupModel({
      id: 'hasCustomStyle',
      label: 'Has custom style',
      options: [
        {
            label: 'Yes',
            value: true,
        },
        {
            label: 'No',
            value: false
        }
    ],
    }),
      new DynamicInputModel({
        id: 'joinLimit',
        disabled: true,
        inputType: 'number',
        hint: 'Default: 10 user',
        max: 100,
        min: 0,
        label: 'Joiners limit',
        placeholder: 'limit number'
      }),
      new DynamicInputModel({
        id: 'seasonId',
        disabled: true,
        label: 'Season id',
        placeholder: 'uuid'
      }),
      new DynamicSelectModel<string>({
        id: 'seasonName',
        label: 'Season name',
        options: of([
            {
                label: 'Bajnokok Ligája',
                value: 'Bajnokok Ligája',
            },
            {
                label: 'OTP Bank Liga / NB I.',
                value: 'OTP Bank Liga / NB I.'
            }
        ])
    }),

      new DynamicInputModel({
        id: 'userId',
        disabled: true,
        hidden: true,
        label: 'User id',
        placeholder: 'uuid'
      }),
      new DynamicTextAreaModel(
        {
            id: 'description',
            label: 'Description'
      }),
    ]
  })
]
