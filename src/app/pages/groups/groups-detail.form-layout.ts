export const BOOTSTRAP_SAMPLE_FORM_LAYOUT = {

  'createdAt': {
      element: {
          container: 'form-row',
          label: 'col-form-label '
      },
      grid: {
          control: 'col-sm-9',
          errors: 'col-sm-offset-3 col-sm-9',
          label: 'col-form-label col-sm-3'
      }
  },
  'featured': {
        element: {
            container: 'form-row skin skin-flat',
            label: 'col-form-label ',
            option: 'btn-secondary'
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'groupId': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'groupName': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'brandcolor': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'hasCover': {
        element: {
            container: 'form-row',
            label: 'col-form-label ',
            option: 'btn-secondary'
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'cover_file': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'logo': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'logo_file': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'groupType': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'hasCustomStyle': {
        element: {
            container: 'form-row',
            label: 'col-form-label ',
            option: 'btn-primary'
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3',

        }
    },
    'joinLimit': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'seasonName': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'seasonId': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'updatedAt': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
    'description': {
        element: {
            container: 'form-row',
            label: 'col-form-label '
        },
        grid: {
            control: 'col-sm-9',
            errors: 'col-sm-offset-3 col-sm-9',
            label: 'col-form-label col-sm-3'
        }
    },
}
