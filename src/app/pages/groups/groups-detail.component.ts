import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { GroupsService } from '../../services/groups/groups.service'
import { Router, ActivatedRoute } from '@angular/router'
import { UserLoginService } from '../../services/user/user-login.service'
import { LoggedInCallback } from '../../services/user/cognito.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

import { MY_FORM_MODEL } from './groups-detail.form-model'
import { BOOTSTRAP_SAMPLE_FORM_LAYOUT } from './groups-detail.form-layout'

import {
  DynamicFormControlModel,
  DynamicFormService,
  DynamicFormLayout,
  DynamicInputModel
} from '@ng-dynamic-forms/core'

const ONLY_SUPERADMIN_DETAILS = [
  'createdAt',
  'groupId',
  'updatedAt',
  'seasonId',
  'userId'
]

@Component({
  selector: 'app-groups-detail',
  templateUrl: './groups-detail.component.html',
  styleUrls: ['./groups-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GroupsDetailComponent implements LoggedInCallback {
  formModel: DynamicFormControlModel[] = MY_FORM_MODEL
  formGroup: FormGroup

  formLayout: DynamicFormLayout = BOOTSTRAP_SAMPLE_FORM_LAYOUT
  public reader = new FileReader()
  public isLoading = true
  public isEditing: BehaviorSubject<any> = new BehaviorSubject(null)

  public coverImage: DynamicInputModel

  constructor(
    private fb: FormBuilder,
    private groupsService: GroupsService,
    public router: Router,
    public route: ActivatedRoute,
    public userService: UserLoginService,
    private formService: DynamicFormService
  ) {
    this.userService.isAuthenticated(this)

    this.formGroup = this.formService.createFormGroup(this.formModel)

    const coverFileInput = this.formService.findById('cover_file', this.formModel) as DynamicInputModel
    coverFileInput.valueUpdates.subscribe(value => this.readUrl(value))
  }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (
      !isLoggedIn ||
      !userGroups ||
      (userGroups && userGroups.indexOf('Admin') === -1)
    ) {
      this.router.navigate(['/login'])
    } else {
      // console.log('scanning groups');
      this.route.params.subscribe(params => {
        this.groupsService.getGroupsById(
          params['groupId'],
          +params['date'],
          groupData => {
            for (const itemKey in groupData) {
              if (itemKey) {
                const inputModel = this.formService.findById(
                  itemKey,
                  this.formModel
                ) as DynamicInputModel
                // console.log('key', groupData[itemKey]);

                inputModel.valueUpdates.next(groupData[itemKey])
              }
            }

            this.isLoading = false
          }
        )
      })
    }
  }

  resetForm(): void {
    this.isEditing.next(false)
  }

  hasAccess(key: string): boolean {
    if (ONLY_SUPERADMIN_DETAILS.indexOf(key) === -1) {
      return true
    }
    return false
  }

  cancel(): void {
    this.isEditing.next(false)
  }

  save(): void {
    this.isEditing.next(false)
  }

  edit(): void {
    this.isEditing.next(true)
  }

  readUrl(files) {
    if (files) {
      this.reader = new FileReader()

      this.reader.onload = (event: any) => {
        this.coverImage = event.target.result
      }

      this.reader.readAsDataURL(files[0])
    }
    }
}
