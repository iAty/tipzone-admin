import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { GroupsRoutingModule } from './groups-routing.module'
import { GroupsComponent } from './groups.component'
import { SharedModule } from '../../shared/shared.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { GroupsService } from '../../services/groups/groups.service'
import { GroupsDetailComponent } from './groups-detail.component'

import { DynamicFormsCoreModule } from '@ng-dynamic-forms/core'
import { DynamicFormsNGBootstrapUIModule } from '@ng-dynamic-forms/ui-ng-bootstrap'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GroupsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    DynamicFormsCoreModule.forRoot(),
    DynamicFormsNGBootstrapUIModule
  ],
  declarations: [GroupsComponent, GroupsDetailComponent],
  providers: [
    GroupsService
  ]
})
export class GroupsPageModule { }
