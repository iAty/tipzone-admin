import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { GroupsComponent } from './groups.component'
import { GroupsDetailComponent } from './groups-detail.component'

const routes: Routes = [
  {
    path: '',
     component: GroupsComponent,
      data: {
        title: 'Groups Page'
      },
  },
  {
    path: ':groupId',
     component: GroupsDetailComponent,
      data: {
        title: 'Groups Detail Page'
      },
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupsRoutingModule { }
