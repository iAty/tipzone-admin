import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { GroupsComponent } from './groups.component'
import { DynamoDBService } from '../../services/user/ddb.service';
import { CognitoUtil } from '../../services/user/cognito.service';
import { UserLoginService } from '../../services/user/user-login.service';
import { SharedModule } from '../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GroupsService } from '../../services/groups/groups.service';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { LoginComponent } from '../login/login.component';

describe('GroupsComponent', () => {
  let component: GroupsComponent
  let fixture: ComponentFixture<GroupsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes(
          [{path: 'login', component: LoginComponent}]
        ),
        SharedModule,

      ],
      declarations: [ GroupsComponent, LoginComponent ],
      providers: [
        DynamoDBService,
        CognitoUtil,
        GroupsService,
        UserLoginService,
        NgbModalStack
      ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
