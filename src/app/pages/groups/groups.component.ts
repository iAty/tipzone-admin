import { Component, OnInit } from '@angular/core'
import { GroupsService } from '../../services/groups/groups.service'
import {Router} from '@angular/router'
import { UserLoginService } from '../../services/user/user-login.service'
import { LoggedInCallback } from '../../services/user/cognito.service'
import {
  NgbModal,
  ModalDismissReasons,
  NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements LoggedInCallback {
  public logdata: Array<any> = []

  constructor(
    private groupsService: GroupsService,
    public router: Router,
    public userService: UserLoginService,
    private modalService: NgbModal) {
    this.userService.isAuthenticated(this)
  }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (!isLoggedIn || (userGroups && userGroups.indexOf('Admin') === -1)) {
        this.router.navigate(['/login'])
    } else {
        // console.log('scanning groups');
        this.groupsService.getGroups(this.logdata)
    }
  }

  createNewGroup(content: any): void {
    this.modalService.open(content)
  }


}
