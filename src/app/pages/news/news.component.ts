import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  AfterViewInit
} from '@angular/core'
import { Router } from '@angular/router'
import { UserLoginService } from '../../services/user/user-login.service'
import { UserParametersService } from '../../services/user/user-parameters.service'
import { CognitoUtil } from '../../services/user/cognito.service'
import {
  NgbModal,
  ModalDismissReasons,
  NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap'
import { NewsService } from '../../services/news/news.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import {
  DynamicFormControlModel,
  DynamicFormService,
  DynamicFormLayout,
  DynamicInputModel
} from '@ng-dynamic-forms/core'
import { NEWS_FORM_MODEL } from './news-form.model'
import { NEWS_FORM_LAYOUT } from './news-form.layout'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { GroupsService } from '../../services/groups/groups.service'

@Component({
  selector: 'tipzone-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewsComponent implements OnInit {
  reorderable = true
  loadingIndicator = true

  public news: any = []
  public groups: Array<any> = []

  closeResult: string

  constructor(
    private newsService: NewsService,
    private router: Router,
    public userService: UserLoginService,
    private groupsService: GroupsService,
    private modalService: NgbModal,
    public cognitoUtil: CognitoUtil
  ) {
    this.userService.isAuthenticated(this)
    console.log('SecureHomeComponent: constructor')

  }

  ngOnInit() {}

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (!isLoggedIn || (userGroups && userGroups.indexOf('Admin') === -1)) {
      this.router.navigate(['/login'])
    } else {
      this.loadingIndicator = true
      this.newsService.getNewsByUser().then( data => {
        this.news = data

        setTimeout( () => {
          this.loadingIndicator = false
        }, 1000)
      }).catch( hasError => {
        console.warn(hasError)
        setTimeout( () => {
          this.loadingIndicator = false
        }, 1000)
      })
      this.groupsService.getGroups(this.groups).then( data => this.groups = data).catch(hasError => console.warn(hasError))
    }
  }

  getRowClass(row) {
    return {
      'bg-success': row.gender === 'female'
    }
  }

  open(ev: Event, item?: any) {
    ev.preventDefault()
    const modalRef = this.modalService.open(NgbdModalContentComponent)
    console.log('hey', item.groupId, item.userId)
    const editedItem = (item && !item.title) ? new News(item.groupId, item.userId) : item
    modalRef.componentInstance.item = editedItem
    modalRef.componentInstance.isNew = (item && !item.title)

    modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`
        this.loadingIndicator = true
        this.newsService.getNewsByUser().then( data => {
          this.news = data
          setTimeout( () => {
            this.loadingIndicator = false
          }, 1000)
        }).catch( hasError => {
          console.warn(hasError)
          setTimeout( () => {
            this.loadingIndicator = false
          }, 1000)
        })

    }, (reason) => {
      this.loadingIndicator = true
      this.newsService.getNewsByUser().then( data => {
        this.news = data
        setTimeout( () => {
          this.loadingIndicator = false
        }, 1000)
      }).catch( hasError => {
        console.warn(hasError)
        setTimeout( () => {
          this.loadingIndicator = false
        }, 1000)
      })
      this.closeResult = `Dismissed`

    })


  }


}

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header" >
      <h4 class="modal-title">{{!isNew ? 'Edit' : 'Create'}} news</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

        <form [formGroup]="newsFormGroup">
          <div class="row justify-content-md-center">
            <div class="col-12">
              <dynamic-ng-bootstrap-form [group]="newsFormGroup" [layout]="newsFormLayout"
              [model]="newsFormModel"></dynamic-ng-bootstrap-form>
            </div>
          </div>
          <hr/>
          <button type="button" class="btn btn-warning" (click)="cancel()">Cancel</button>
          <button type="submit"  class="btn" [class.btn-success]="isNew" [class.btn-info]="!isNew"
          (click)="save()">{{!isNew ? 'Save' : 'Create'}}</button>
    </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
   </div>
  `
})
export class NgbdModalContentComponent implements AfterViewInit {
  @Input() item
  @Input() isNew
  public newsFormModel: DynamicFormControlModel[] = NEWS_FORM_MODEL
  public newsFormGroup: FormGroup

  public newsFormLayout: DynamicFormLayout = NEWS_FORM_LAYOUT
  isLoading = true
  constructor(
    public activeModal: NgbActiveModal,
    private formService: DynamicFormService,
    private newsService: NewsService,
  ) {
    this.newsFormGroup = this.formService.createFormGroup(this.newsFormModel)
  }

  ngAfterViewInit() {
    this.handler()
  }

  handler(): void {
    if (this.item) {
      for (const itemKey in this.item) {
        if (itemKey) {
          console.log(itemKey)
        const inputModel = this.formService.findById(
          itemKey,
          this.newsFormModel
        ) as DynamicInputModel
        inputModel.valueUpdates.next(this.item[itemKey])
        }
      }

      this.isLoading = false
    } else {
      this.newsFormGroup.reset()
    }
  }

  save(): void {
    this.newsService.createNews(this.newsFormGroup.get('group').value, () => this.activeModal.dismiss())

  }

  create(): void {
    this.newsService.createNews(this.newsFormGroup.get('group').value, () => this.activeModal.dismiss())
  }

  cancel(): void {
    this.newsFormGroup.reset()

    this.activeModal.dismiss()
  }
}

export class News {
  title: string = null
  content: string = null
  updateAt: number = null
  userId: string = null
  groupId: string = null
  constructor(groupId: string, userId: string) {
    this.groupId = groupId
    this.userId = userId
  }
}
