import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { NewsRoutingModule } from './news-routing.module'
import { NewsComponent, NgbdModalContentComponent } from './news.component'
import { SharedModule } from '../../shared/shared.module'
import { UserParametersService } from '../../services/user/user-parameters.service'

import { DynamicFormsCoreModule } from '@ng-dynamic-forms/core'
import { DynamicFormsNGBootstrapUIModule } from '@ng-dynamic-forms/ui-ng-bootstrap'

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NewsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        DynamicFormsCoreModule.forRoot(),
        DynamicFormsNGBootstrapUIModule
    ],
    entryComponents: [
      NgbdModalContentComponent
    ],
    declarations: [
      NewsComponent,
      NgbdModalContentComponent
    ],
    providers: [
      UserParametersService
    ]
})
export class NewsPageModule { }
