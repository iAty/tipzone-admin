import {
  DynamicFormControlModel,
  DynamicCheckboxModel,
  DynamicInputModel,
  DynamicSelectModel,
  DynamicRadioGroupModel,
  DynamicFormGroupModel,
  DynamicTextAreaModel
} from '@ng-dynamic-forms/core'
import { of } from 'rxjs/observable/of'

export const NEWS_FORM_MODEL: DynamicFormControlModel[] = [
  new DynamicFormGroupModel({
    id: 'group',
    legend: 'Group',
    group: [
      new DynamicInputModel({
        id: 'groupId',
        // hidden: true,
        label: 'groupID',
        placeholder: 'UUID'
      }),
      new DynamicInputModel({
        id: 'groupName',
        // hidden: true,
        label: 'groupName',
        placeholder: 'name'
      }),
      new DynamicInputModel({
        id: 'userId',
        // hidden: true,
        label: 'userId',
        placeholder: 'UUID'
      }),
      new DynamicInputModel({
        id: 'createdAt',
        // hidden: true,
        label: 'Created At',
        placeholder: 'date'
      }),
      new DynamicInputModel({
        id: 'updateAt',
        label: 'Updated at',
        placeholder: 'date'
      }),
      new DynamicInputModel({
        id: 'title',
        label: 'Title',
        placeholder: 'title'
      }),
      new DynamicTextAreaModel({
        id: 'content',
        label: 'Content',
        placeholder: 'content'
      })
    ]
  })
]
