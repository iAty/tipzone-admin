import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UserLoginService } from '../../services/user/user-login.service'
import { LoggedInCallback } from '../../services/user/cognito.service'
import { TriggersStore } from '../../services/triggers/triggers'

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements LoggedInCallback {

  public UpdatedEventsLogs: Array<any> = []
  public CalculateUserScoreEventsLogs: Array<any> = []
  public TrucanteUserEventsLogs: Array<any> = []
  public isLoading = false

  constructor( public router: Router,
    public userService: UserLoginService,
    private triggers: TriggersStore) {
      this.UpdatedEventsLogs = [
        {
          name: 'Results success updated',
          count: 1
        }
      ]

      this.CalculateUserScoreEventsLogs = [
        {
          name: 'UPDATE',
          status: 'success',
          results: [
            {
              userId: '123',
              userScore: 3
            }
          ]
        }
      ]
    }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (!isLoggedIn || (userGroups && userGroups.indexOf('Admin') === -1)) {
        this.router.navigate(['/dashboard'])
    } else {
        console.log('scanning groups')

    }
  }

  updateDataResults(): void {
    this.isLoading = true
    this.triggers.runResultsUpdater()
      .then(success => console.log('success results', success))
      .catch(hasError => console.warn('has result error', hasError))
  }

}


