import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { EventsRoutingModule } from './events-routing.module'
import { EventsComponent } from './events.component'
import { SharedModule } from '../../shared/shared.module'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    EventsRoutingModule
  ],
  declarations: [EventsComponent],
  exports: [EventsComponent]
})
export class EventsPageModule { }
