import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { EventsComponent } from './events.component'

const routes: Routes = [
  {
    path: '',
     component: EventsComponent,
    data: {
      title: 'Fixed Navbar Footer Layout Page'
    },
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
