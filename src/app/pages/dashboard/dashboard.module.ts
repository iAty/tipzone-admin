import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { DashboardRoutingModule } from './dashboard-routing.module'
import { DashboardComponent } from './dashboard.component'
import { SharedModule } from '../../shared/shared.module'
import { UserParametersService } from '../../services/user/user-parameters.service'



@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        DashboardRoutingModule,
        FormsModule
    ],
    declarations: [
      DashboardComponent
    ],
    providers: [
      UserParametersService
    ]
})
export class DashboardPageModule { }
