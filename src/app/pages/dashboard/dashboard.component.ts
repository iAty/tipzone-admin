import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UserLoginService } from '../../services/user/user-login.service'
import { UserParametersService } from '../../services/user/user-parameters.service'
import { CognitoUtil, Callback } from '../../services/user/cognito.service'

@Component({
  selector: 'tipzone-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  reorderable = true
  loadingIndicator = true

  rows = [
    { name: 'Austin', gender: 'Male', company: 'Swimlane' },
    { name: 'Dany', gender: 'Male', company: 'KFC' },
    { name: 'Molly', gender: 'Female', company: 'Burger King' },
  ]
  columns = [
    { prop: 'name' },
    { name: 'Gender' },
    { name: 'Company' }
  ]

  constructor(private router: Router, public userService: UserLoginService, private userParams: UserParametersService,
    public cognitoUtil: CognitoUtil) {
    this.userService.isAuthenticated(this)
    console.log('SecureHomeComponent: constructor')
  }

  ngOnInit() {
  }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (!isLoggedIn || (userGroups && userGroups.indexOf('Admin') === -1)) {
        this.router.navigate(['/login'])
    }
  }

  getRowClass(row) {
    return {
      'bg-success': (row.gender === 'female')
    }
  }

}
