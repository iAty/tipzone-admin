import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { DynamoDBService } from '../../services/user/ddb.service'
import { UserLoginService } from '../../services/user/user-login.service'
import { ChallengeParameters, CognitoCallback, LoggedInCallback } from '../../services/user/cognito.service'

@Component({
  selector: 'tipzone-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements CognitoCallback, LoggedInCallback, OnInit {
  email: string
  password: string
  errorMessage: string
  mfaStep = false
  mfaData = {
    destination: '',
    callback: null
  }
  isLoading = true

  constructor(public router: Router,
    public ddb: DynamoDBService,
    public userService: UserLoginService) {
    console.log('LoginComponent constructor')
  }

  ngOnInit() {
    this.errorMessage = null
    console.log('Checking if the user is already authenticated. If so, then redirect to the secure site')
    this.userService.isAuthenticated(this)
    setTimeout(() => {
      this.isLoading = false
    }, 600)
  }

  onLogin() {
    if (this.email == null || this.password == null) {
      this.errorMessage = 'All fields are required'
      return
    }
    this.errorMessage = null
    this.userService.authenticate(this.email, this.password, this)
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
      this.errorMessage = message
      console.log('result: ' + this.errorMessage)
      if (this.errorMessage === 'User is not confirmed.') {
        console.log('redirecting')
        this.router.navigate(['/home/confirmRegistration', this.email])
      } else if (this.errorMessage === 'User needs to set password.') {
        console.log('redirecting to set new password')
        this.router.navigate(['/home/newPassword'])
      }
    } else { // success
      this.ddb.writeLogEntry('login')
      this.router.navigate(['/dashboard'])
    }
  }

  handleMFAStep(challengeName: string, challengeParameters: ChallengeParameters, callback: (confirmationCode: string) => any): void {
    this.mfaStep = true
    this.mfaData.destination = challengeParameters.CODE_DELIVERY_DESTINATION
    this.mfaData.callback = (code: string) => {
      if (code == null || code.length === 0) {
        this.errorMessage = 'Code is required'
        return
      }
      this.errorMessage = null
      callback(code)
    }
  }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (isLoggedIn && (userGroups && userGroups.indexOf('Admin') > -1)) {
      this.router.navigate(['/dashboard'])
    }
  }

  cancelMFA(): boolean {
    this.mfaStep = false
    return false   // necessary to prevent href navigation
  }

}


@Component({
  selector: 'awscognito-angular2-app',
  template: ''
})
export class LogoutComponent implements LoggedInCallback {

  constructor(public router: Router,
              public userService: UserLoginService) {
      this.userService.isAuthenticated(this)
  }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
      if (isLoggedIn) {
          this.userService.logout()
          this.router.navigate(['/login'])
      }

      this.router.navigate(['/login'])
  }
}
