import {Component} from '@angular/core'
import {UserRegistrationService} from '../../services/user/user-registration.service'
import {CognitoCallback} from '../../services/user/cognito.service'
import {Router} from '@angular/router'
@Component({
    selector: 'awscognito-angular2-app',
    template: `
    <form class="form-signin" method="POST" action="#" role="form">
    <div class="form-group">
        <h2>Resend Code</h2>
    </div>
    <div *ngIf="errorMessage!=null" class="alert alert-danger">
        {{ errorMessage }}
    </div>
    <div class="form-group">
        <label class="control-label" for="signupEmail">Email</label>
        <input id="signupEmail" type="email" maxlength="50" class="form-control" [(ngModel)]="email"
               [ngModelOptions]="{standalone: true}">
    </div>
    <div class="form-group">
        <button (click)="resendCode()" id="signupSubmit" type="submit" class="btn btn-info btn-block">
            Resend Code
        </button>
    </div>

    <hr>
    <p><a [routerLink]="['/home/register']"> <i class="fa fa-fw fa-group"></i> Register</a> <a
            [routerLink]="['/home/login']"> <i class="fa fa-fw fa-user"></i> Login</a>
    </p>
</form>
    `
})
export class ResendCodeComponent implements CognitoCallback {

    email: string
    errorMessage: string

    constructor(public registrationService: UserRegistrationService, public router: Router) {

    }

    resendCode() {
        this.registrationService.resendCode(this.email, this)
    }

    cognitoCallback(error: any, result: any) {
        if (error != null) {
            this.errorMessage = 'Something went wrong...please try again'
        } else {
            this.router.navigate(['/confirmRegistration', this.email])
        }
    }
}
