import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { LoginComponent, LogoutComponent } from './login.component'
import { RouterModule } from '@angular/router'
import { SharedModule } from '../../shared/shared.module'
import { ResendCodeComponent } from './resend.component'
import { UserRegistrationService } from '../../services/user/user-registration.service'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: 'login', component: LoginComponent, data: { title: 'Login page', children: [] } },
      { path: 'logout', component: LogoutComponent },
      { path: 'resend', component: ResendCodeComponent },
      { path: 'forgotPassword', component: ResendCodeComponent },
    ]),
    FormsModule
  ],
  declarations: [
    LoginComponent,
    LogoutComponent,
    ResendCodeComponent
  ],
  exports: [
    LoginComponent,
    LogoutComponent,
    ResendCodeComponent,
    RouterModule
  ],
  providers: [
    UserRegistrationService
  ]
})
export class LoginModule { }
