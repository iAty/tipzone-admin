import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { UserLoginService } from '../../services/user/user-login.service'
import { LoggedInCallback } from '../../services/user/cognito.service'
import { TriggersStore } from '../../services/triggers/triggers'

@Component({
  selector: 'tipzone-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements LoggedInCallback {


  public isLoading = false

  constructor( public router: Router,
    public userService: UserLoginService,
    private triggers: TriggersStore) {

    }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    if (!isLoggedIn || (userGroups && userGroups.indexOf('Admin') === -1)) {
        this.router.navigate(['/dashboard'])
    } else {
        console.log('scanning groups')

    }
  }

  updateDataResults(): void {
    this.isLoading = true
    this.triggers.runResultsUpdater()
      .then(success => console.log('success results', success))
      .catch(hasError => console.warn('has result error', hasError))
  }

}


