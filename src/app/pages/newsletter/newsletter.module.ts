import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { NewsletterRoutingModule } from './newsletter-routing.module'
import { NewsletterComponent } from './newsletter.component'
import { SharedModule } from '../../shared/shared.module'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NewsletterRoutingModule
  ],
  declarations: [NewsletterComponent],
  exports: [NewsletterComponent]
})
export class NewsletterPageModule { }
