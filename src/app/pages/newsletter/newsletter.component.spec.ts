import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing';
import { NewsletterComponent } from './newsletter.component'
import { UserLoginService } from '../../services/user/user-login.service';
import { DynamoDBService } from '../../services/user/ddb.service';
import { CognitoUtil } from '../../services/user/cognito.service';
import { TriggersStore } from '../../services/triggers/triggers';
import { AmplifyService } from 'aws-amplify-angular';

describe('NewsletterComponent', () => {
  let component: NewsletterComponent
  let fixture: ComponentFixture<NewsletterComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ NewsletterComponent ],
      providers: [
        UserLoginService,
        DynamoDBService,
        CognitoUtil,
        TriggersStore,
        AmplifyService
      ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsletterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
