import {Component, OnInit} from '@angular/core'
import { LoggedInCallback, CognitoUtil } from './services/user/cognito.service'
import { UserLoginService } from './services/user/user-login.service'
import { AwsUtil } from './services/user/aws.service'


@Component({
    selector: 'tipzone-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, LoggedInCallback {

  constructor(public awsUtil: AwsUtil, public userService: UserLoginService, public cognito: CognitoUtil) {}


  ngOnInit() {
    console.log('AppComponent: Checking if the user is already authenticated')
    this.userService.isAuthenticated(this)
  }

  isLoggedIn(message: string, isLoggedIn: boolean, userGroups?: Array<string>) {
    console.log(message + ' ' + isLoggedIn)
    const mythis = this
    this.cognito.getIdToken({
        callback() {

        },
        callbackWithParam(token: any) {
            // Include the passed-in callback here as well so that it's executed downstream
            console.log('AppComponent: calling initAwsService in callback')
            mythis.awsUtil.initAwsService(null, isLoggedIn, token)
        }
    })
}
}
