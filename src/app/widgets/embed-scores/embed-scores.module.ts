import { NgModule } from '@angular/core'
import { EmbedScoresWidgetComponent } from './embed-scores.component'
import { RouterModule } from '@angular/router'


const EMBED_SCORES_WIDGET = [
  {
    path: 'embed/scores',
    component: EmbedScoresWidgetComponent
  }
]

@NgModule({
  imports: [RouterModule.forRoot(EMBED_SCORES_WIDGET)],
  exports: [RouterModule],
  declarations: [
    EmbedScoresWidgetComponent
  ],
  providers: [],
})
export class EmbedScoresWidgetModule { }
