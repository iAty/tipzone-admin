
import { NgModule } from '@angular/core'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AppRoutingModule } from './app-routing.module'
import { SharedModule } from './shared/shared.module'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppComponent } from './app.component'


import { AuthService } from './shared/auth/auth.service'
import { AuthGuard } from './shared/auth/auth-guard.service'
import { SuperAuthGuard } from './shared/auth/super-auth-guard.service'

import { EmbedScoresWidgetModule } from './widgets/embed-scores/embed-scores.module'


import * as $ from 'jquery'
import { FixedNavbarFooterLayoutComponent } from './layouts/fixed-navbar-footer-layout/fixed-navbar-footer-layout.component'

import { AwsUtil } from './services/user/aws.service'
import { CognitoUtil } from './services/user/cognito.service'
import { UserLoginService } from './services/user/user-login.service'
import { DynamoDBService } from './services/user/ddb.service'
import { GroupsService } from './services/groups/groups.service'
import { LoginModule } from './pages/login/login.module'
import { UseractivityComponent } from './secure/useractivity/useractivity.component'
import { NewsService } from './services/news/news.service'
import { TriggersStoreProvider, TriggersStore } from './services/triggers/triggers'
// import { Sigv4Http } from './services/sig4.service';

import { AmplifyAngularModule, AmplifyService } from 'aws-amplify-angular'


@NgModule({
    declarations: [
        AppComponent,
        FixedNavbarFooterLayoutComponent,
        UseractivityComponent
    ],
    imports: [
        LoginModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModule,
        NgbModule.forRoot(),
        EmbedScoresWidgetModule,
        AmplifyAngularModule
    ],
    providers: [
        AuthService,
        AuthGuard,
        SuperAuthGuard,
        AwsUtil,
        CognitoUtil,
        UserLoginService,
        DynamoDBService,
        GroupsService,
        NewsService,
        TriggersStoreProvider,
        TriggersStore,
        // Sigv4Http,
        AmplifyService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

