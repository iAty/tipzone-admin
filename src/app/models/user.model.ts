export class RegistrationUser {
  name: string
  email: string
  phone_number: string
  password: string
}

export class NewPasswordUser {
  username: string
  existingPassword: string
  password: string
}
