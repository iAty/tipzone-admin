// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  region: 'eu-west-2',

    identityPoolId: 'eu-west-2:1d367ab2-2626-4f2f-919b-b0b37731c284',
    userPoolId: 'eu-west-2_LQo9X82RC',
    clientId: '6vrajo4gjm4kddvutt161rttob',

    rekognitionBucket: 'rekognition-pics',
    albumName: 'usercontent',
    bucketRegion: 'eu-west-2',
    userFilesBucket: 'tipzone-userfiles-mobilehub-1836564323',

    ddbTableName: 'tipzone-mobilehub-1836564323-LoginTrail',
    GROUPS_TABLE_NAME: 'tipzone-mobilehub-1836564323-sport-groups',
    GROUP_NEWS_TABLE: 'tipzone-mobilehub-1836564323-group-news',
    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    dynamodb_endpoint: '',
    s3_endpoint: '',
    USER_CONFIG_API_ENDPOINT: 'https://g5wasff0yi.execute-api.eu-west-2.amazonaws.com/Development',
}
